# natgeo-wic

![](https://img.shields.io/badge/written%20in-C%2B%2B-blue)

a WIC codec to parse image National Geographic magazine scans.

## Description

"The Complete National Geographic" is a DVD box set containing tens of GB of magazine scans dating back to the 1800's. Unfortunately the images are stored in an obfuscated `.cng` file format and mandate a proprietary viewer, whereas Windows users expect seamless integration with platform utilities such as Windows Photo Viewer and Windows Explorer. The format was deciphered in the past, but the available tooling was limited to converting the files, consuming time and space. This project allows you to view the scans without any up-front conversion.

- Supports viewing `.cng` images within Windows Photo Viewer
- Supports thumbnails for Windows Explorer
- Read-only integration with any WIC-based imaging software
- 32-bit and 64-bit builds

## Installing

- Install C++ Runtimes (Visual Studio 2013)
- Open Command Prompt as Administrator
- `regsvr32 path/to/natgeo-wic.dll`
- Double-click register_format.reg
- Test the sample file!

## License

The file `nanojpg.cpp` is distributed under the KeyJ Research License. The sample CNG file is copyright National Geographic. Other source code files are available under the ISC or MIT licenses.

Thanks to the MIT-licensed `dds-wic-codec` project, where several files originate from:
https://code.google.com/p/dds-wic-codec/

Thanks to Blaze Montanuh's website for discussion on file formats:
http://www.subdude-site.com/WebPages_Local/RefInfo/Computer/Linux/LinuxGuidesByBlaze/appsImagePhotoTools/cng2jpgGuide/cng2jpg_guide.htm


## Download

- [⬇️ natgeo-wic_1.0.7z](dist-archive/natgeo-wic_1.0.7z) *(285.07 KiB)*
